package com.epam.homework.yuriibobkov.airport.model;

import java.util.Objects;

public class Ticket {
    private Long id;
    private Double price;
    private Passenger passenger;
    private Flight flight;

    public Ticket() {
    }

    public Ticket(Long id, Double price, Passenger passenger, Flight flight) {
        this.id = id;
        this.price = price;
        this.passenger = passenger;
        this.flight = flight;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(id, ticket.id) &&
                Objects.equals(price, ticket.price) &&
                Objects.equals(passenger, ticket.passenger) &&
                Objects.equals(flight, ticket.flight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price, passenger, flight);
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", price=" + price +
                ", passenger=" + passenger +
                ", flight=" + flight +
                '}';
    }
}

