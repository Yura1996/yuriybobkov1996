package com.epam.homework.yuriibobkov.airport.model;

public enum Cities {
    NEWYORK(Countries.USA),
    BEIJING(Countries.CHINA),
    BERLIN(Countries.GERMANY),
    PARIS(Countries.FRANCE),
    TOKIO(Countries.JAPAN);
    private Countries country;

    Cities(Countries country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "City=" + this.name() +
                " country=" + country;
    }
}
