package com.epam.homework.yuriibobkov.airport.model;

import java.time.LocalDateTime;
import java.util.*;


public class Flight {
    private Long id;
    private LocalDateTime departure;
    private LocalDateTime arrival;
    private Cities departureCity;
    private Cities arrivalCity;
    private Aviacompany aviacompany;
    private Map<Cities, List<Ticket>> ticketsByCities;

    public Flight() {
        this.ticketsByCities = new EnumMap<>(Cities.class);
    }

    public Flight(
            Long id,
            LocalDateTime departure,
            LocalDateTime arrival,
            Cities departureCity,
            Cities arrivalCity,
            Aviacompany aviacompany) {
        this.id = id;
        this.departure = departure;
        this.arrival = arrival;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.aviacompany = aviacompany;
        this.ticketsByCities = new EnumMap<>(Cities.class);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDeparture() {
        return departure;
    }

    public void setDeparture(LocalDateTime departure) {
        this.departure = departure;
    }

    public LocalDateTime getArrival() {
        return arrival;
    }

    public void setArrival(LocalDateTime arrival) {
        this.arrival = arrival;
    }

    public Cities getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(Cities departureCity) {
        this.departureCity = departureCity;
    }

    public Cities getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(Cities arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Aviacompany getAviacompany() {
        return aviacompany;
    }

    public void setAviacompany(Aviacompany aviacompany) {
        this.aviacompany = aviacompany;
    }

    public Map<Cities, List<Ticket>> getTicketsByCities() {
        return ticketsByCities;
    }

    public void setTicketsByCities(Map<Cities, List<Ticket>> ticketsByCities) {
        this.ticketsByCities = ticketsByCities;

    }

    public void insertTicketsByCities(Ticket ticket) {
        if (ticket != null) {
            Cities city = ticket.getFlight().departureCity;
            if (ticketsByCities.containsKey(city)) {
                List<Ticket> tickets = ticketsByCities.get(city);
                tickets.add(ticket);
                ticketsByCities.put(city, tickets);
            } else {
                List<Ticket> tickets = new ArrayList<>();
                tickets.add(ticket);
                ticketsByCities.put(ticket.getFlight().getDepartureCity(), tickets);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return Objects.equals(id, flight.id) &&
                Objects.equals(departure, flight.departure) &&
                Objects.equals(arrival, flight.arrival) &&
                departureCity == flight.departureCity &&
                arrivalCity == flight.arrivalCity &&
                Objects.equals(aviacompany, flight.aviacompany);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, departure, arrival, departureCity, arrivalCity, aviacompany);
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", departure=" + departure +
                ", arrival=" + arrival +
                ", departureCity=" + departureCity +
                ", arrivalCity=" + arrivalCity +
                ", aviacompany=" + aviacompany +
                '}';
    }
}
