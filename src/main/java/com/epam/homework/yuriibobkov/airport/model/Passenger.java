package com.epam.homework.yuriibobkov.airport.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Passenger {
    private Long id;
    private String firstName;
    private String lastName;
    private String birthDate;
    private Boolean sex;
    private String numberID;
    private Set<Ticket> tickets;

    public Passenger() {
        tickets = new HashSet<>();
    }

    public Passenger(Long id, String firstName, String lastName, String birthDate, Boolean sex, String numberID) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.sex = sex;
        this.numberID = numberID;
        tickets = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getNumberID() {
        return numberID;
    }

    public void setNumberID(String numberID) {
        this.numberID = numberID;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(Ticket ticket) {
        if (ticket.getPassenger().equals(this)) {
            tickets.add(ticket);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passenger passenger = (Passenger) o;
        return Objects.equals(id, passenger.id) &&
                Objects.equals(firstName, passenger.firstName) &&
                Objects.equals(lastName, passenger.lastName) &&
                Objects.equals(birthDate, passenger.birthDate) &&
                Objects.equals(sex, passenger.sex) &&
                Objects.equals(numberID, passenger.numberID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, birthDate, sex, numberID);
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", sex=" + sex +
                ", numberID='" + numberID + '\'' +
                '}';
    }
}
