package com.epam.homework.yuriibobkov.airport.model;

import java.time.LocalDateTime;
import java.util.*;

public class Aviacompany {
    private Long id;
    private String name;
    private LocalDateTime expirationDate;
    private Set<Flight> flights;

    public Aviacompany() {
        flights = new TreeSet<>();
    }

    public Aviacompany(Long id, String name, LocalDateTime expirationDate) {
        this.id = id;
        this.name = name;
        this.expirationDate = expirationDate;
        flights = new TreeSet<>(Comparator.comparing(Flight::getId));
    }

    public Set<Flight> getFlights() {
        return flights;
    }

    public void setFlights(Set<Flight> flights) {
        this.flights = flights;
    }

    public void insertFlights(Flight flight) {
        this.flights.add(flight);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aviacompany that = (Aviacompany) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(expirationDate, that.expirationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, expirationDate);
    }

    @Override
    public String toString() {
        return "Aviacompany{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
